local keymap = vim.api.nvim_set_keymap
local opts = { noremap = true }

-- Leader
vim.g.mapleader = ' '

-- Window Movement
keymap('n', '<Space>', '<NOP>', opts)
keymap('n', '<c-j>', '<c-w>j', opts)
keymap('n', '<c-h>', '<c-w>h', opts)
keymap('n', '<c-k>', '<c-w>k', opts)
keymap('n', '<c-l>', '<c-w>l', opts)

-- Visual Mode Tabbing
keymap('v', '<', '<gv', opts)
keymap('v', '>', '>gv', opts)

-- V-Mode Move Selected Line/Text
keymap('v', 'J', ":move '>+1<CR>gv-gv", opts)
keymap('v', 'K', ":move '<-2<CR>gv-gv", opts)

-- Tabs
keymap('n', '<TAB>', ':bnext<CR>', opts)
keymap('n', '<S-TAB>', ':bprevious<CR>', opts)
keymap('n', '<c-w>', ':bd<CR>', opts)

-- NvimTree
keymap('n', '<Leader>e', ':NvimTreeToggle<CR>', opts)
